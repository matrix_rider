/* all_tests.c
//
// Copyright (C) 2009 Elena Grassi <grassi.e@gmail.com>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
*/

#include <stdio.h>
#include "CuTest.h"
 CuSuite * fasta_get_suite();
CuSuite * matrix_get_suite();
void RunAllTests(void)
{
	CuString * output = CuStringNew();
	CuSuite * suite = CuSuiteNew();
	CuSuiteAddSuite(suite, fasta_get_suite());
	CuSuiteAddSuite(suite, matrix_get_suite());
	CuSuiteRun(suite);
	CuSuiteSummary(suite, output);
	CuSuiteDetails(suite, output);
	printf("%s\n", output->buffer);
} int main(void)
{
	RunAllTests();
	return 0;
}
