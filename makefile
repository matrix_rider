DEST?=$(BIOINFO_ROOT)/binary/$(BIOINFO_HOST)/local
LDFLAGS+=-lm
OBJ = matrix_rider

.PHONY: all clean install

all: matrix_rider

clean:
	rm -f  $(OBJ)

install: $(OBJ)
	install -D -s $< $(DEST)/bin/$<

matrix_rider: fasta.c main.c matrixes.c CuTest.c input.c
	gcc $(LDFLAGS) $(CFLAGS) -o $@ $^

matrix_rider_debug: fasta.c main.c matrixes.c CuTest.c input.c debug.c
	gcc -Wall -g -lm -DDEBUG -o $@ $^

cuTest: all_tests.c fasta.c  CuTest.c input.c matrixes.c tests.c
	gcc -lm -g -o $@ $^
