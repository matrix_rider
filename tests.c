/* tests.c
//
// Copyright (C) 2009 Elena Grassi <grassi.e@gmail.com>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "total_affinity.h"
#include "CuTest.h"

/* fasta tests */

void test_assign_bg(CuTest * tc)
{
	run info = (run) calloc(1, sizeof(struct run_));
	info->window = DEFAULT_WINDOW_SIZE;
	info->revcomp = DEFAULT_REVCOMP;
	info->counts = DEFAULT_COUNT;
	info->cutoff = DEFAULT_CUTOFF;
	info->abs_cutoff = DEFAULT_ABS_CUTOFF;
	info->given_abs_cutoff = 0;
	info->error = 0;
	fasta f = (fasta) calloc(1, sizeof(struct fas));
	char *gat = "GATTACA";
	f->seq = strdup(gat);
	f->length = strlen(gat);
	assign_zero_bg(f);
	int i = 0;
	for (i = 0; i < BASES; i++)
		CuAssertDblEquals(tc, 0, f->background[i], 0);
	while (*gat != '\0') {
		add_count_bg(f, *gat, info);
		gat++;
	}
	CuAssertDblEquals(tc, 3, f->background[A], 0);
	CuAssertDblEquals(tc, 1, f->background[C], 0);
	CuAssertDblEquals(tc, 1, f->background[G], 0);
	CuAssertDblEquals(tc, 2, f->background[T], 0);
	assign_bg(f, info);
	CuAssertIntEquals(tc, 0, info->error);
	CuAssertDblEquals(tc, 0.428571, f->background[A], EPSILON);
	CuAssertDblEquals(tc, 0.142857, f->background[C], EPSILON);
	CuAssertDblEquals(tc, 0.142857, f->background[G], EPSILON);
	CuAssertDblEquals(tc, 0.285714, f->background[T], EPSILON);
	CuAssertIntEquals(tc, 7, f->ACGTlength);
	free(info);
}

void test_assign_bg_N(CuTest * tc)
{
	run info = (run) calloc(1, sizeof(struct run_));
	info->window = DEFAULT_WINDOW_SIZE;
	info->revcomp = DEFAULT_REVCOMP;
	info->counts = DEFAULT_COUNT;
	info->cutoff = DEFAULT_CUTOFF;
	info->abs_cutoff = DEFAULT_ABS_CUTOFF;
	info->given_abs_cutoff = 0;
	info->error = 0;
	fasta f = (fasta) calloc(1, sizeof(struct fas));
	char *gat = "GATTACANN";
	f->seq = strdup(gat);
	f->length = strlen(gat);
	assign_zero_bg(f);
	int i = 0;
	for (i = 0; i < BASES; i++)
		CuAssertDblEquals(tc, 0, f->background[i], 0);
	while (*gat != '\0') {
		add_count_bg(f, *gat, info);
		gat++;
	}
	CuAssertDblEquals(tc, 3, f->background[A], 0);
	CuAssertDblEquals(tc, 1, f->background[C], 0);
	CuAssertDblEquals(tc, 1, f->background[G], 0);
	CuAssertDblEquals(tc, 2, f->background[T], 0);
	assign_bg(f, info);
	CuAssertIntEquals(tc, 0, info->error);
	CuAssertDblEquals(tc, 0.428571, f->background[A], EPSILON);
	CuAssertDblEquals(tc, 0.142857, f->background[C], EPSILON);
	CuAssertDblEquals(tc, 0.142857, f->background[G], EPSILON);
	CuAssertDblEquals(tc, 0.285714, f->background[T], EPSILON);
	CuAssertIntEquals(tc, 7, f->ACGTlength);
	free(info);
}

void test_get_rc(CuTest * tc)
{
	char *torc = "AAGATTACA";
	char *rc = (char *)calloc(8, sizeof(char));
	get_rc(torc, 2, 7, rc);
	CuAssertStrEquals(tc, "TGTAATC", rc);
	free(rc);
}

void test_get_rc_N(CuTest * tc)
{
	char *torc = "GANTA";
	char *rc = (char *)calloc(strlen(torc) + 1, sizeof(char));
	get_rc(torc, 0, strlen(torc), rc);
	CuAssertStrEquals(tc, "TANTC", rc);
	free(rc);
}

CuSuite *fasta_get_suite()
{
	CuSuite *suite = CuSuiteNew();
	SUITE_ADD_TEST(suite, test_assign_bg);
	SUITE_ADD_TEST(suite, test_assign_bg_N);
	SUITE_ADD_TEST(suite, test_get_rc);
	SUITE_ADD_TEST(suite, test_get_rc_N);
	return suite;
}

/* matrixes tests */

void test_get_matrixes_counts(CuTest * tc)
{
	char *matrix_test_file = "./examples/CuTestMatrix";
	run info = (run) calloc(1, sizeof(struct run_));
	info->counts = DEFAULT_COUNT;
	info->f_matrixes = fopen(matrix_test_file, "r");
	if (!info->f_matrixes) {
		printf("Error while opening test file %s\n", matrix_test_file);
		CuAssertTrue(tc, 0);
		return;
	}
	int n_mat = 0;
	get_matrixes(info);
	CuAssertIntEquals(tc, 0, info->error);
	CuAssertIntEquals(tc, 2, info->n_matrixes);
	CuAssertIntEquals(tc, 4, (info->matrixes)[0]->length);
	int i;
	int mat = 0;
	CuAssertIntEquals(tc, 4, (info->matrixes)[mat]->length);
	for (i = 0; i < (info->matrixes)[mat]->length; i++) {
		CuAssertDblEquals(tc, 0.5, (info->matrixes)[mat]->freq[i][A],
				  0);
		CuAssertDblEquals(tc, 0.166667,
				  (info->matrixes)[mat]->freq[i][C], EPSILON);
		CuAssertDblEquals(tc, 0.166667,
				  (info->matrixes)[mat]->freq[i][G], EPSILON);
		CuAssertDblEquals(tc, 0.166667,
				  (info->matrixes)[mat]->freq[i][T], EPSILON);
	}
	mat = 1;
	CuAssertIntEquals(tc, 7, (info->matrixes)[mat]->length);
	fclose(info->f_matrixes);
	free(info);
}

void test_affinity(CuTest * tc)
{
	matrix_ll m = (matrix_ll) calloc(1, sizeof(struct matrix_ll_));
	m->ll = (double **)calloc(4, sizeof(double *));
	m->length = 4;
	int i;
	for (i = 0; i < m->length; i++) {
		m->ll[i] = (double *)calloc(4, sizeof(double));
		m->ll[i][A] = 1;
		m->ll[i][C] = -0.58496;
		m->ll[i][G] = -0.58496;
		m->ll[i][T] = -0.58496;
	}

	char *s = "AAAAGGGGCCCCTTTT";
	CuAssertDblEquals(tc, -1.509760, get_affinity(m, s, 2), EPSILON);
	CuAssertDblEquals(tc, 1.660160, get_affinity(m, s, 12), EPSILON);
	double results[2];
	get_affinities(m, s, 0, results);
	CuAssertDblEquals(tc, 4, results[0], 0);
	CuAssertDblEquals(tc, -2.339840, results[1], EPSILON);
	free(m);
}

void test_matrix_run(CuTest *tc)
{
	run info = NULL;
	set_beginning_info(&info);
	matrix_ll m = (matrix_ll) calloc(1, sizeof(struct matrix_ll_));
	m->ll = (double **)calloc(4, sizeof(double *));
	m->length = 4;
	int i;
	for (i = 0; i < m->length; i++) {
		m->ll[i] = (double *)calloc(4, sizeof(double));
		m->ll[i][A] = 1;
		m->ll[i][C] = -0.584963;
		m->ll[i][G] = -0.584963;
		m->ll[i][T] = -0.584963;
	}
	fasta f = (fasta) calloc(1, sizeof(struct fas));
	char *gat = "AAAAGGGGCCCCTTTTNNNN";
	f->seq = strdup(gat);
	f->length = strlen(gat);
	f->ACGTlength = f->length;
	int hm = 0;
	double *ll = matrix_run(m, f, &hm, info);
	CuAssertIntEquals(tc, 0, info->error);
	CuAssertIntEquals(tc, 17, hm);
	CuAssertDblEquals(tc, 1, ll[0], EPSILON);
	CuAssertDblEquals(tc, 1, ll[12], EPSILON);
	CuAssertDblEquals(tc, 3.738158, matrix_little_window_tot(m, f, -1, -1, &hm),
			  EPSILON);
	free(m);
	free(f);
	free(info);
}

void test_from_assign_ll_to_little_window(CuTest * tc)
{
	run info = (run) calloc(1, sizeof(struct run_));
	matrix_ll m = (matrix_ll) calloc(1, sizeof(struct matrix_ll_));
	m->freq = (double **)calloc(5, sizeof(double *));
	m->length = 5;
	int i;
	for (i = 0; i < m->length; i++) {
		m->freq[i] = (double *)calloc(4, sizeof(double));
		m->freq[i][A] = 5;
		m->freq[i][C] = 5;
		m->freq[i][G] = 5;
		m->freq[i][T] = 10;
	}
	fasta f = (fasta) calloc(1, sizeof(struct fas));
	char *gat = "ACGTTTTTAAAA";
	f->seq = strdup(gat);
	f->length = strlen(gat);
	assign_zero_bg(f);
	for (i = 0; i < f->length; i++)
		add_count_bg(f, f->seq[i], info);
	CuAssertDblEquals(tc, 5, f->background[A], 0);
	CuAssertDblEquals(tc, 1, f->background[C], 0);
	CuAssertDblEquals(tc, 1, f->background[G], 0);
	CuAssertDblEquals(tc, 5, f->background[T], 0);
	assign_bg(f, info);
	CuAssertIntEquals(tc, f->length, f->ACGTlength);
	CuAssertDblEquals(tc, 0.416667, f->background[A], EPSILON);
	CuAssertDblEquals(tc, 0.083333, f->background[C], EPSILON);
	CuAssertDblEquals(tc, 0.083333, f->background[G], EPSILON);
	CuAssertDblEquals(tc, 0.416667, f->background[T], EPSILON);
	info->log2 = 0;		/*first we test without the log
				   assign_ll(m, f, info); */

}

CuSuite *matrix_get_suite()
{
	CuSuite *suite = CuSuiteNew();
	SUITE_ADD_TEST(suite, test_get_matrixes_counts);
	SUITE_ADD_TEST(suite, test_affinity);
	SUITE_ADD_TEST(suite, test_matrix_run);
	SUITE_ADD_TEST(suite, test_from_assign_ll_to_little_window);
	return suite;
}
