/* debug.c
//
// Copyright (C) 2009 Elena Grassi <grassi.e@gmail.com>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
*/
#include <stdio.h>
#include "total_affinity.h"

void print_fasta(run info)
{
	int m = 0;
	for (m = 0; m < info->n_fasta; m++) {
		printf(">%s\n%s\n", (info->fas)[m]->id, (info->fas)[m]->seq);
		printf("percent: %g %g %g %g %d %d\n",
		       (info->fas)[m]->background[A],
		       (info->fas)[m]->background[C],
		       (info->fas)[m]->background[G],
		       (info->fas)[m]->background[T], (info->fas)[m]->length,
		       (info->fas)[m]->ACGTlength);
	}
}

void print_matrixes(run info)
{
	int n, h, g = 0;
	for (h = 0; h < info->n_matrixes; h++) {
		for (n = 0; n < (info->matrixes)[h]->length; n++) {
			for (g = 0; g < BASES; g++) {
				printf("%s\t%lf\t%lf\t%lf\t%lf\t%d\n",
				       (info->matrixes)[h]->name,
				       (info->matrixes)[h]->ll[n][g],
				       (info->matrixes)[h]->freq[n][g],
				       (info->matrixes)[h]->cutoff,
				       (info->matrixes)[h]->frac_cutoff,
				       (info->matrixes)[h]->length);
			}
		}
	}
}
