/* main.c
//
// Copyright (C) 2008 Elena Grassi <grassi.e@gmail.com>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "total_affinity.h"

/*
    Function: main
    
    Drives the execution following command line arguments.
    
    Parameters:
    
    argc - int (default): the number of given arguments.
    argv - char** (default): array of arguments strings.  
*/
int main(int argc, char *argv[])
{

	run info = NULL;
	if (set_beginning_info(&info))
		return ERROR;

	parse_command_line(argv, argc, info);

#ifdef DEBUG
	printf("#Starting with window %d, revcomp %d, count %d, cutoff %lf, "
	       "abs_cutoff %lf, given_abs_cutoff %d, log2 %d, slide %d, only_max %d "
	       "error_status %d\n",
	       info->window, info->revcomp, info->counts, info->cutoff,
	       info->abs_cutoff, info->given_abs_cutoff, info->log2,
	       info->slide, info->only_max, info->error);
#endif				/* DEBUG */

	check_error(info);

	if (info->window == 0 && !info->miRNA)	/* if in syteseeker mode we always use loglikelihoods. */
		info->log2 = 1;

	get_matrixes(info);
	check_error(info);

	if (info->f_cutoffs) {
		get_cutoffs(info);
		check_error(info);
	}
#ifdef DEBUG
	printf("#Loaded %d matrixes\n", info->n_matrixes);
	print_matrixes(info);
#endif				/* DEBUG */

	load_fasta(info);
	check_error(info);

#ifdef DEBUG
	printf("#Loaded %d fasta\n", info->n_fasta);
	print_fasta(info);
#endif				/* DEBUG */

	int i, j = 0;
	double *d = NULL;
	int how_many = 0;
	for (i = 0; i < info->n_fasta; i++) {
		printf(">%s\n", (info->fas)[i]->id);

		if (!i || !info->f_background) {
			for (j = 0; j < info->n_matrixes; j++) {
				/* !i because at least we have to get ll foreach matrix at the beginning.
				   !info->f_background because if we have different bg for every 
				   fasta we have to do this everytime and not foreach matrix only. */
				assign_ll((info->matrixes)[j], (info->fas)[i],
					  info);
				check_error(info);
			}
		}

		if (info->window != 0) {
			if (info->window != -1) {
				//sliding window mode
				int W = info->window;
				int J = info->slide;
				strong_assert(W % J == 0);
				int Csize = W/J;
				int N_mat = info->n_matrixes;
				char *seq = (info->fas)[i]->seq;//check the pointer
				int seq_len = (info->fas)[i]->length;
				strong_assert(seq_len >= W);

				int p = 0;
				int j = 0;
				for(j=0; j < N_mat; j++){
					matrix_ll m = (info->matrixes)[j];
					for(p=0; p <= W-2*J; p+=J){
						int tot_match_noN=0;
						int corner = p + J + m->length -1 <  seq_len ?  p + J + m->length -1 : seq_len -1;

						(m->cache)[p/J + 1]  = matrix_little_window_tot(m, info->fas[i], p                   , p + J , &tot_match_noN, info);
						(m->normaliz_cache)[p/J + 1]  = tot_match_noN;
						(m->cache)[p/J + 1] += matrix_little_window_tot(m, info->fas[i], p + J - m->length +1, corner, &tot_match_noN, info);
						(m->normaliz_cache)[p/J + 1] += tot_match_noN;
						// the first index was not initialized, before using cache we scroll them
					}
				}
				
				p=0;
				while (p <= seq_len - W) {
					for (j = 0; j < N_mat; j++) {
						matrix_ll m = info->matrixes[j];
						int tot_match_noN=0;
						
						/* cache scroll        
							consier unsing fifo instead of array 
							or even better an object with a fifo and a running sum
						*/
						int c = 0;
						for(c=0; c<Csize-1; c++){
							((m)->cache)[c]=((m)->cache)[c+1];
							((m)->normaliz_cache)[c]=((m)->normaliz_cache)[c+1];
						}
						/************************/
						
						int corner = p + W + m->length -1 < seq_len ? p + W + m->length -1 : seq_len -1;
 						double portion_aff_corner_case = matrix_little_window_tot(m, info->fas[i], p + W - m->length +1, corner, &tot_match_noN, info);
						((m)->normaliz_cache)[Csize-1] = tot_match_noN;
     						double portion_aff             = matrix_little_window_tot(m, info->fas[i], p + W - J,             p + W,  &tot_match_noN, info);
						((m)->cache)[Csize-1]           = portion_aff + portion_aff_corner_case;
						((m)->normaliz_cache)[Csize-1] += tot_match_noN;
						//the preceding protions are in the cache
						
						double tot_aff = 0;
						for(c=0; c < Csize-1; c++){
							tot_aff+=((m)->cache)[c];
						}
						tot_aff += portion_aff;

						int tot_match_noN_sum = 0;
						for(c=0; c < Csize-1; c++){
							tot_match_noN_sum += ((m)->normaliz_cache)[c];
						}
						tot_match_noN_sum += tot_match_noN; // the last tot_match_noN computed are those of te portions without corner cases

						if(tot_match_noN_sum == 0 && info->skip_N_regions){
							break;
						}
						if(info->normalize_on_seq_len){
							tot_aff/=tot_match_noN_sum;
						}
						printf("%d\t%s\t%g\n", 
							p, (m)->name, tot_aff); 
					}
					p+=J;
				}
				
				/* old way
				printf("%s\n",
				       (info->matrixes)[j]->name);
				d = matrix_run((info->matrixes)[j],
					       (info->fas)[i],
					       &how_many, info);
				check_error(info);
				if (d)
					print_window_affinities(d, info->window - info->matrixes[j]->length + 1, how_many, info->slide);
				free(d);
				*/

			} else {
				// info->window == -1 (single window mode)
				for (j = 0; j < info->n_matrixes; j++) {
					if ((!i || !info->f_background) && info->miRNA) { //we need a cutoff for miRNA mode. 
						assign_cutoff((info->matrixes)[j], info);
					}
					//printf("%s\t",
					//       (info->matrixes)[j]->name);
					//printf("%g\n",
					//       matrix_little_window_tot((info->matrixes)[j], (info->fas)[i]->seq, -1, -1, info));
					int tot_match_noN = 0;
					double tot_aff = matrix_little_window_tot((info->matrixes)[j], (info->fas)[i], 0, -1, &tot_match_noN, info);
					if(info->normalize_on_seq_len){
						tot_aff/=tot_match_noN;
					}
					printf("%s\t%g\n", (info->matrixes)[j]->name, tot_aff);
				}
			}
		} else {
			//single site mode
			for (j = 0; j < info->n_matrixes; j++) {
				if (!i || !info->f_background) {
					assign_cutoff((info->matrixes)[j],
						      info);
				}
				if (info->miRNA)
					find_single_strand_matches((info->matrixes)[j],
		    								   (info->fas[i]), info);
				else
					matrix_cutoff_print((info->matrixes)[j],
						    			(info->fas[i]), info);
				check_error(info);
			}
		}
	}
#ifdef DEBUG
	printf("#Loaded %d matrixes\n", info->n_matrixes);
	print_matrixes(info);
#endif				/* DEBUG */

	tidy(info);
	return OK;
}
