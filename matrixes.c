/* matrixes.c
//
// Copyright (C) 2008 Elena Grassi <grassi.e@gmail.com>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include "total_affinity.h"
#include "CuTest.h"

//XXX fix Paolo's format
/*
    Function: get_matrixes
        Loads matrixes from the given file, putting their infos in info.
    
    Parameters:
        info - pointer to run struct with a properly opened FILE* with matrixes 
               (Paolo's format), info->count tells if we already have percentages 
               [0] or pseudocounts [1] or counts [2] of single nucleotides.
               Error flags will be put there.
        
*/
void get_matrixes(run info)
{
	int cache_size = 0;
	if(info->window !=0 && info->window != -1){
		assert(info->window % info->slide == 0);
		cache_size = info->window / info->slide;
	}
	if (alloc_matrixes(&(info->matrixes),cache_size))
		info->error = MEMORY_ERROR;
	matrix_ll *matrixes = info->matrixes;
	char name[MAX_MATRIX_NAME];
	char old_name[MAX_MATRIX_NAME];
	double acgt[BASES];
	strcpy(old_name, "");
	strcpy(name, "");
	int bases_c = 0;
	int length = 0;
	int ongoing = 1;
	int i = 0;
	while (ongoing) {
		if (fscanf(info->f_matrixes, "%s\t%*d\t%lf\t%lf\t%lf\t%lf\n",
			   name, &acgt[A], &acgt[C], &acgt[G], &acgt[T]) != 5) {
#ifdef DEBUG
			printf("You fool fscanf!\n");
#endif				/* DEBUG */
			if (fgetc(info->f_matrixes) != EOF)
				info->error = MATRIX_FILE_ERROR;
			ongoing = 0;
			break;	/* XXX BAD */
		}
		if (strcmp(name, old_name)) {	/* we have a new matrix */
#ifdef DEBUG
			printf("Last was %d, now adding %s\n", length, name);
#endif				/* DEBUG */
			if (info->n_matrixes != 0) {
				(*matrixes)->length = length;	/* assign length to last matrix */
				get_fractions_from_pcounts(*matrixes, info);
				check_error(info);
				matrixes++;	/* next matrix to be loaded */
			}
			info->n_matrixes++;
			bases_c = 0;
			length = 0;
			strcpy((*matrixes)->name, name);
		}
		for (i = 0; i < BASES; i++)
			(*matrixes)->freq[length][i] = acgt[i];
		length++;
		if (info->n_matrixes >= MAX_MATRIXES
		    || length >= MAX_MATRIX_LENGTH) {
			ongoing = 0;
			info->error = MATRIX_LIMIT_ERROR;
		}
		strcpy(old_name, name);
	}
	(*matrixes)->length = length;	/* assign length to last matrix */
	get_fractions_from_pcounts(*matrixes, info);
}

//XXX
void get_cutoffs(run info)
{
	double abs_c = 0.0;
	double frac_c = 0.0;
	int i, c = 0;
	char name[MAX_MATRIX_NAME];
	int found[info->n_matrixes];
	for (; i < info->n_matrixes; i++)
		found[i] = 0;
	/* matrix name \t absolute_cutoff \t fractional_cutoff */
	while (fscanf
	       (info->f_cutoffs, "%s\t\t%lf\t%lf\n", name, &abs_c,
		&frac_c) == 3) {
		if (frac_c > 1 || frac_c < 0) {
			fprintf(stderr,
				"WARNING: invalid fractional cutoff for %s matrix!\n	",
				name);
			info->error = CUTOFF_FILE_ERROR;
			return;
		}
		int matrix_index = find_matrix_index(info, name);
		if (matrix_index == -1) {
			/* We issue a warning for unknown matrixes. */
			fprintf(stderr,
				"WARNING: matrix %s in the cutoff file is not in the matrix file, ignoring it\n",
				name);
		} else if (found[matrix_index]) {
			/* We exit if a double matrix is found. */
			fprintf(stderr,
				"WARNING: matrix %s in the cutoff file is doubled!\n",
				name);
			info->error = CUTOFF_FILE_ERROR;
			return;
		} else {
			c++;
			found[matrix_index] = 1;
			info->matrixes[matrix_index]->abs_cutoff = abs_c;
			info->matrixes[matrix_index]->frac_cutoff = frac_c;
		}
	}
	if (fgetc(info->f_cutoffs) != EOF) {
		info->error = CUTOFF_FILE_ERROR;
		return;
	}
	if (c != info->n_matrixes) {
		fprintf(stderr,
			"WARNING: Some matrixes are missing in the cutoff file:\n");
		for (i = 0; i < info->n_matrixes; i++) {
			if (!found[i]) {
				fprintf(stderr, "%s\n",
					info->matrixes[i]->name);
			}
		}
		info->error = CUTOFF_FILE_ERROR;
		return;
	}
}

//XXX
int find_matrix_index(run info, char *name)
{
	/* Non-duplicate matrix names is a prerequisite. */
	/* Use an hash to be more efficient. */
	int i = 0;
	for (; i < info->n_matrixes; i++) {
		if (!strcmp(info->matrixes[i]->name, name))
			return i;
	}
	return -1;
}

/*
    Function: get_fractions_from_pcounts.
    
    Get nucleotides frequencies for a given matrix_ll with counts or pseudocounts 
    stored in the freq array and store there the frequency.
    If count evaluates true (default) 0 values are converted to 1 (pseudocount), 
    otherwise (perc to be implemented, TODO).
    Issues an error in info->error if it finds a zero total for counts in the
    given matrix_ll.
    
    Parameters:
        m - matrix_ll with matrixes data.
        info - run with the desired count. 
*/
/* possible gain of speed getting ACGT total in get_matrixes */
void get_fractions_from_pcounts(matrix_ll m, run info)
{
	double tot = 0.0;
	int i, j = 0;
	for (j = 0; j < m->length; j++) {
		for (i = 0; i < BASES; i++) {
			int val = (int)m->freq[j][i];
			if (info->counts && val != m->freq[j][i]) {	//then it's not an integer 
				info->error = MATRIX_COUNT_ERROR;
				return;
			}
			if (m->freq[j][i] <= EEEPSILON && !info->keep_zeroes) {
				if (info->counts) {
					m->freq[j][i] = 1;
				} else { 
					info->error = FREQ_ZERO_ERROR;
					return;
				}
			}
			tot += (int)m->freq[j][i];
		}
		
		if (info->counts) {
			if (!tot) {
				info->error = MATRIX_COUNT_ERROR;
				return;
			}
			for (i = 0; i < BASES; i++) {
				m->freq[j][i] = m->freq[j][i] / tot;
			}
		}
		tot = 0.0;
	}
}

/* 
    Function: normalize_fractions

    TODO
*/

/*
    Function: assign_ll
    
    Computes log-likelihood given a fasta with background informations and a
    matrix_ll with freq loaded (counts already converted to fractions).
	This function does not compute affinities, it's only a preprocess step.    	
	
    Parameters:
        m - matrix_ll with freq loaded and missing ll.
        f - fasta with background informations to compute log likelihoods.
        info - pointer to run struct to set ad error flag if there is a zero or 
               negative freq (or background)
*/
void assign_ll(matrix_ll m, fasta f, run info)
{
	int j = 0;
	int i = 0;
	for (; j < m->length; j++)
		for (i = 0; i < BASES; i++) {
			if (!info->miRNA)
				m->ll[j][i] = log2_ratio(m->freq[j][i], f->background[i], info);
			else
				m->ll[j][i] = m->freq[j][i];
		}
}

/*
    Function: assign_cutoff

    Assign a cutoff to a matrix_ll using the fractional cutoff 
    in the given run. If given_abs_cutoff evaluates true it uses the 
    weakest cutoff (the smallest) between abs and fractional.
    
    Parameters:
        m - matrix_ll with ll loaded and missing cutoff.
        info - run with cutoff informations.
*/
void assign_cutoff(matrix_ll m, run info)
{
	int j = 0;
	int i = 0;
	double max_tot = 0;
	double max;
	int mmmax;
	for (; j < m->length; j++) {
		max = m->ll[j][0];
		for (i = 1; i < BASES; i++) {
			if (m->ll[j][i] > max) {
				max = m->ll[j][i];
				mmmax=i;
			}
		}
		max_tot += max;
	}

	/* If a file with cutoffs is given we will use them, otherwise we have default
	   or given parameters equals for every matrix. */
	if (info->f_cutoffs) {
		info->cutoff = m->frac_cutoff;
		info->abs_cutoff = m->abs_cutoff;
	}
	m->cutoff = max_tot * info->cutoff;

	if ((info->given_abs_cutoff || info->f_cutoffs)
	    && m->cutoff > info->abs_cutoff)
		m->cutoff = info->abs_cutoff;
	
	if (info->miRNA) { /* We count matches for miRNA seeking mode. */
		m->cutoff = max_tot;	
	}
}

/*  Function: log2_ratio
    
    Returns the log2ratio or only the ratio (depending on info->log2),
    sets error to 1 if the ratio is non-positive.
    Computes log2ratio if info->log2 evaluates true, ratio otherwise.
    
    Parameters:
        n - double which will be used as numerator.
        d - double which will be used as denominator.
        info - run used to set error flags and determine if log has to be applied.
        
    Return:
        double which is the log2-ratio or simple ratio between n and d.
        (will be 0 if an error occurred).
*/

double log2_ratio(double n, double d, run info)
{
	if (d < EPSILON) {
		info->error = BACKGROUND_FREQ_ERROR;
		return 0;
	}
	double ratio = n / d;
	if (ratio <= 0 && !(ratio == 0 && info->keep_zeroes)) {
		info->error = BACKGROUND_FREQ_ERROR;
		return 0;
	}
	if (info->log2)
		return log(ratio) / log(2);
	else 
		return ratio;
}

/*
    Function: alloc_matrixes
    
    Creates space (calloc) for all the matrixes that will be loaded.
    
    Parameters:
        m - matrix_ll ** where the matrixes will be stored.
    
    Returns:
        ERROR (1) if any calloc call failed, OK (0) otherwise.
*/
int alloc_matrixes(matrix_ll ** m, int cache_size)
{
	*m = (matrix_ll *) calloc(MAX_MATRIXES + 1, sizeof(matrix_ll));
	if (!(*m)) {
		free(*m);
		return ERROR;
	}
	int i = 0;
	for (i = 0; i < MAX_MATRIXES; i++) {
		(*m)[i] = (matrix_ll) calloc(1, sizeof(struct matrix_ll_));
		if (!((*m)[i])) {
			free_matrixes(*m, i);
			return ERROR;
		}
		(*m)[i]->ll =
		    (double **)calloc(MAX_MATRIX_LENGTH + 1, sizeof(double *));
		if (!((*m)[i]->ll)) {
			free_matrixes(*m, i);
			return ERROR;
		}
		(*m)[i]->freq =
		    (double **)calloc(MAX_MATRIX_LENGTH + 1, sizeof(double *));
		if (!((*m)[i]->freq)) {
			free_matrixes(*m, i);
			return ERROR;
		}
		(*m)[i]->cache = NULL;
		if(cache_size!=0){
			(*m)[i]->cache =
			    (double *)calloc(cache_size, sizeof(double));
			if (!((*m)[i]->cache)) {
				free_matrixes(*m, i);
				return ERROR;
			}
		}
		(*m)[i]->normaliz_cache = NULL;
		if(cache_size!=0){
			(*m)[i]->normaliz_cache =
			    (int *)calloc(cache_size, sizeof(double));
			if (!((*m)[i]->normaliz_cache)) {
				free_matrixes(*m, i);
				return ERROR;
			}
		}
		double **cur = NULL;
		for (cur = (*m)[i]->ll; cur < (*m)[i]->ll + MAX_MATRIX_LENGTH;
		     cur++) {
			*cur = (double *)calloc(BASES, sizeof(double));
			if (!(*cur)) {
				free_matrixes(*m, i);
				return ERROR;
			}
		}
		for (cur = (*m)[i]->freq;
		     cur < (*m)[i]->freq + MAX_MATRIX_LENGTH; cur++) {
			*cur = (double *)calloc(BASES, sizeof(double));
			if (!(*cur)) {
				free_matrixes(*m, i);
				return ERROR;
			}
		}
		(*m)[i]->name = (char *)calloc(MAX_MATRIX_NAME, sizeof(char));
		if (!((*m)[i]->name)) {
			free_matrixes(*m, i);
			return ERROR;
		}
	}
	return OK;
}

/*
    Function: free_matrixes
    
        Frees the space used by all the loaded matrixes.
    
    Parameters:
    
        m - matrix_ll * with the matrixes to be freed.
        loaded - number of matrixes to be freed.
*/
void free_matrixes(matrix_ll * m, int loaded)
{
	int i = 0;
	for (i = 0; i < loaded; i++) {
		free(m[i]->name);
		double **cur = NULL;
		for (cur = m[i]->ll; cur < m[i]->ll + MAX_MATRIX_LENGTH; cur++)
			free(*cur);
		free(m[i]->ll);
		for (cur = m[i]->freq; cur < m[i]->freq + MAX_MATRIX_LENGTH; cur++)
			free(*cur);
		free(m[i]->freq);
	    free(m[i]->cache);
	    free(m[i]->normaliz_cache);
		free(m[i]);
	}
	free(m);
}

/*
    Function: get_affinity
    
        Computes affinity between a matrix and a slice of a string, given the first char to be evaluated. 
        Assumes enough space in the string. Returns total values ("straight" + revcomp).
    
    Parameters:
    
        m - matrix_ll matrix that will be used.
        s - char * versus which matrix will be compared.
        start - int first position for affinity computation.
    
    Returns:
    
        double with the total affinity (sum of all log_likelihoods) between
        the given matrix and string.
*/
double get_affinity(matrix_ll m, char *s, int start)
{
	double tot = 0;
	int i = 0;
	while (i < m->length) {
		/* switch(toupper(s[i])) { //toupper done while loading fasta for efficiency's sake */
		switch (s[start]) {
		case 'A':
			tot += m->ll[i][A] + m->ll[(m->length) - i - 1][T];
			break;
		case 'C':
			tot += m->ll[i][C] + m->ll[(m->length) - i - 1][G];
			break;
		case 'G':
			tot += m->ll[i][G] + m->ll[(m->length) - i - 1][C];
			break;
		case 'T':
			tot += m->ll[i][T] + m->ll[(m->length) - i - 1][A];
			break;
		case 'N':
			tot += 2 * N;
			break;
		}
		i++;
		start++;
	}
	return tot;
}

/*
    Function: get_affinities
    
        Computes affinity between a matrix and a slice of a string, given the first char to be evaluated. 
        Assumes enough space in the string. Puts results in the given double array ("straight" and revcomp 
        are evaluated at the same time).
    
    Parameters:
    
        m - matrix_ll matrix that will be used.
        s - char * versus which matrix will be compared.
        start - int first position for affinity computation.
        results - double * where affinities will be put.    
*/
void get_affinities(matrix_ll m, char *s, int start, double *results)
{
	/* results[0] is straight total, results[1] revcomp */
	int i = 0;
	results[0] = 0;
	results[1] = 0;
	while (i < m->length) {
		switch (s[start]) {
		case 'A':
			results[0] += m->ll[i][A];
			results[1] += m->ll[(m->length) - i - 1][T];
			break;
		case 'C':
			results[0] += m->ll[i][C];
			results[1] += m->ll[(m->length) - i - 1][G];
			break;
		case 'G':
			results[0] += m->ll[i][G];
			results[1] += m->ll[(m->length) - i - 1][C];
			break;
		case 'T':
			results[0] += m->ll[i][T];
			results[1] += m->ll[(m->length) - i - 1][A];
			break;
		case 'N':
			results[0] += N;
			results[1] += N;
			break;
		}
		i++;
		start++;
	}
}

//XXX
double get_affinities_nonLog(matrix_ll m, char *s, int start, int *foundN, run info)
{
	/* results[0] is straight total, results[1] revcomp */
	int i = 0;
	double results[2];
	results[0] = 1;
	results[1] = 1;
	while (i < m->length) {
		switch (s[start]) {
		case 'A':
			results[0] *= m->ll[i][A];
			results[1] *= m->ll[(m->length) - i - 1][T];
			break;
		case 'C':
			results[0] *= m->ll[i][C];
			results[1] *= m->ll[(m->length) - i - 1][G];
			break;
		case 'G':
			results[0] *= m->ll[i][G];
			results[1] *= m->ll[(m->length) - i - 1][C];
			break;
		case 'T':
			results[0] *= m->ll[i][T];
			results[1] *= m->ll[(m->length) - i - 1][A];
			break;
		case 'N':
			results[0] *= N;
			results[1] *= N;
			(*foundN)++;
			break;
		}
		i++;
		start++;
	}
	if (info->single_strand)
		return results[0];
	else
		return (results[0] > results[1]) ? results[0] : results[1];
}

//XXX
double get_affinities_single_strand(matrix_ll m, char *s, int start, int *foundN, run info)
{
	double res = 0.0;
	int i = 0;
	while (i < m->length) {
		switch (s[start]) {
		case 'A':
			res += m->ll[i][A];
			break;
		case 'C':
			res += m->ll[i][C];
			break;
		case 'G':
			res += m->ll[i][G];
			break;
		case 'T':
			res += m->ll[i][T];
			break;
		case 'N':
			(*foundN)++;
			res += N;
			break;
		}
		i++;
		start++;
	}
	if (fabs(res - m->cutoff) <= EEEPSILON)
		return 1;
    else
    	return 0;
}

/*
    Function: matrix_run
    
    Computes affinity between a matrix and a string on all viable positions.
    
    Parameters:
    
        m - matrix_ll matrix that will be used.
        s - fasta versus which matrix will be compared.
        how_many - int pointer where the number of viable positions will be put. 
        info - run to issue error flags (memory error or fasta too short).
    
    Returns:
    
        double array which stores total affinity of the matrix for each viable
        position on the given string (starts at 0, +1 and evaluates it until there
        are enough chars left in the given string). NULL if a memory error occurs or
        the fasta is too short to find the given matrix.
*/
double *matrix_run(matrix_ll m, fasta s, int *how_many, run info)
{
#ifdef DEBUG
	printf("matrix_run with %s %d\n", m->name, m->length);
#endif				/* DEBUG */
	*how_many = s->length - m->length + 1;
	if (*how_many <= 0) {
		info->error = SHORT_FASTA_WARNING;
		return NULL;
	}
	double *res = (double *)calloc(*how_many, sizeof(double));
	if (!res) {
		info->error = MEMORY_ERROR;
		return NULL;
	}
	int offset = 0;
	while (m->length <= s->length - offset) {
		int dummy = 0;
		res[offset] = info->get_affinities_pointer(m, s->seq, offset, &dummy, info);
#ifdef DEBUG
		printf("Seen affinity %g at offset %d\n", res[offset], offset);
#endif				/* DEBUG */
		offset++;
	}
#ifdef DEBUG
	printf("matrix_run %d %d\n", offset, *how_many);
#endif				/* DEBUG */
	assert(offset == *how_many);
	return res;
}

//XXX
/*
    Function: matrix_little_window_print_tot
    
    Computes affinity between a matrix and a string on all viable positions and
    returns the sum of all affinities.
    
    Parameters:
    
        m   - matrix_ll matrix that will be used.
        seq - sequence versus which matrix will be compared.
		s_length - the

    
    Returns:
       
       double with total affinity between the given matrix and fasta,
       normalized upon the number of matrix matches != 0 (i.e. without an N).
*/
double matrix_little_window_tot(matrix_ll m, fasta f, int begin, int end, int *tot_match_noN, run info)
{
	int offset = 0;
        char *seq = f->seq;
        int l = f->length;
	if(begin > 0){
		if(begin > l){
			fprintf(stderr,	"ERROR: invalid begin in matrix_little_window_tot()\n");
			exit(1);
		}
		offset = begin;
	}
	if(end > 0){
		if(end > l){
			fprintf(stderr,	"ERROR: invalid end in matrix_little_window_tot()\n");
			exit(1);
		}
	}else{
		end = l;
	}

	double tot = 0;
	(*tot_match_noN) = 0;
	while (offset <= end - m->length) {
		int foundN = 0;
		double match = get_affinities_nonLog(m, seq, offset, &foundN, info);
		tot += match;
		//if (!match) { //now we directly count the Ns
		if (!foundN) {
			(*tot_match_noN)++;
		}
		offset++;
	}
	return tot;
}

/*
    Function: print_window_affinities
    
    Prints (on stdout) total affinities for all possible windows, getting infos from a 
    double array which has affinities for a matrix computed for all positions.
    
    Parameters:
    
    tot_aff - double * with the computed affinities for all positions.
    window - int with the wanted window size.
    tot - length of tot_aff array.
    slide - number of bases to be skipped between two outputs.
*/
void print_window_affinities(double *tot_aff, int window, int tot, int slide)
{
	/*int k = 0;
	   for (; k < tot; k++)
	   printf("%d: %g\n", k, tot_aff[k]);
	   printf("Using window %d tot %d\n", window, tot); */
	double tot_w = 0;
	int tot_match_noN = 0;
	int i = 0;
	int w = 0;
	int cw = 0;
	while (i < tot) {
		if (w < window) {
			//printf("For window %d at %d adding %g\n", cw, i, tot_aff[i]);
			if (tot_aff[i]) {
				tot_w += tot_aff[i];
				tot_match_noN++;
			}
			w++;
			i++;
		} else {
			printf("%d\t%g\n", cw++, tot_w / tot_match_noN);
			if (slide < window) {
				int j;
				for (j = (i - window); j < (i + slide - window);
				     j++) {
					//printf("For window %d at %d removing %g\n", cw, j, tot_aff[j]);
					if (tot_aff[j]) {
						tot_w -= tot_aff[j];
						tot_match_noN--;
					}
				}
				w = window - slide;
			} else {
				tot_w = 0;
				tot_match_noN = 0;
				w = 0;
				i += slide - window;
				//printf("Starting for a long jump after w.%d, to %d\n", cw, i);
			}
		}
	}
	if (w == window)
		printf("%d\t%g\n", cw++, tot_w / tot_match_noN);

}

/*
    Function: matrix_cutoff_print
    
    Computes affinity between a matrix and a string on all viable positions, 
    compares with the cutoff and prints the result if they are greater.
    
    Parameters:
    
    m - matrix_ll matrix that will be used.
    s - fasta versus which matrix will be compared.
    info - run with cutoff record that says if we have 
           to print the matched revcomp (1) or straight seq (0).
*/
void matrix_cutoff_print(matrix_ll m, fasta s, run info)
{
#ifdef DEBUG
	printf("matrix_cutoff_print with %s %d\n", m->name, m->length);
#endif				/* DEBUG */
	int offset = 0;
	int max_strand = 1;	/* 1 is +, 0 is - */
	int max_pos = 0;
	int seen_any_match = 0;
	char *plus = "+";
	char *minus = "-";
	char *match = (char *)calloc((m->length) + 1, sizeof(char));
	char *max_match = (char *)calloc((m->length) + 1, sizeof(char));
	if (!match || !max_match) {
		info->error = MEMORY_ERROR;
		return;
	}
	double affinities[2];
	double max_aff = 0;
	while (m->length <= s->length - offset) {
		seen_any_match = 1;
		get_affinities(m, s->seq, offset, affinities);
		if (info->only_max) {
			if (!offset)
				max_aff = affinities[0];
			double max_local = affinities[0];
			int max_strand_local = 1;
			if (affinities[1] > affinities[0]) {
				max_local = affinities[1];
				max_strand_local = 0;
			}
			/* remembers only last higher match */
			if (max_aff <= max_local) {
				max_aff = max_local;
				max_pos = offset;
				max_strand = max_strand_local;
				if (info->revcomp)
					get_rc(s->seq, offset, m->length,
					       max_match);
				else
					get_seq(s->seq, offset, m->length,
						max_match);
			}
		} else {
			if (affinities[0] >= m->cutoff) {
				get_seq(s->seq, offset, m->length, match);
				printf("%s\t%s\t%g\t%d\t%s\n", m->name, match,
				       affinities[0], offset, plus);
			}
#ifdef DEBUG
			else
				printf("BELOW CUTOFF %g %s\t%g\t%d\t%s\n",
				       m->cutoff, m->name, affinities[0],
				       offset, plus);
#endif				/* DEBUG */
			if (affinities[1] >= m->cutoff) {
				if (info->revcomp)
					get_rc(s->seq, offset, m->length,
					       match);
				else	/* very unlikely to have match already there for a plus match for the same matrix? XXX */
					get_seq(s->seq, offset, m->length,
						match);
				printf("%s\t%s\t%g\t%d\t%s\n", m->name, match,
				       affinities[1], offset, minus);
			}
#ifdef DEBUG
			else
				printf("BELOW CUTOFF %g %s\t%g\t%d\t%s\n",
				       m->cutoff, m->name, affinities[1],
				       offset, minus);
#endif				/* DEBUG */
		}
		offset++;
	}
	if (info->only_max && seen_any_match) {
		if (max_strand)
			printf("%s\t%s\t%g\t%d\t%s\n", m->name, max_match,
			       max_aff, max_pos, plus);
		else
			printf("%s\t%s\t%g\t%d\t%s\n", m->name, max_match,
			       max_aff, max_pos, minus);
	}
	free(match);
	free(max_match);
#ifdef DEBUG
	printf("matrix_run %d\n", offset);
#endif				/* DEBUG */
}

//XXX
void find_single_strand_matches(matrix_ll m, fasta s, run info)
{
	int offset = 0;
	char *match = (char *)calloc((m->length) + 1, sizeof(char));
	if (!match) {
		info->error = MEMORY_ERROR;
		return;
	}
	while (m->length <= s->length - offset) {
		int dummy = 0;
		int found = (int) info->get_affinities_pointer(m, s->seq, offset, &dummy, info);
		if (found) {
			get_seq(s->seq, offset, m->length, match); // XXX remove only for testing purposes, inefficient
			printf("%s\t%s\t%d\n", m->name, match, offset);
		}
		offset++;
	}
	free(match);
}
